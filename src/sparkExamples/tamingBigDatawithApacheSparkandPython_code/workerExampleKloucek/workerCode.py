from pyspark.sql.types import *

workingDir = '/tamingBigData/'
workerFile = 'worker.csv'
workplaceFile = 'workplace.csv'

spark = SparkSession.builder.appName('workerTest').enableHiveSupport().getOrCreate()

workerRDD = sc.textFile(workingDir+workerFile)
workplaceRDD = sc.textFile(workingDir+workplaceFile)
					 
workerRDD = workerRDD.map(lambda x:x.split(',')).\
                      map(lambda x:[int(x[0]),x[1].encode('utf8'),x[2].encode('utf8'),x[3].encode('utf8')])					 


workerSchema = StructType([StructField('id',IntegerType(), True),\
                     StructField('name',StringType(), True),\
					 StructField('gender',StringType(), True),\
					 StructField('country',StringType(), True)])
					 
workerDF = spark.createDataFrame(workerRDD,workerSchema)					  

workplaceSchema = StructType([StructField('id',IntegerType(), True),\
                 StructField('department',StringType(), True)])
				 
workplaceRDD = workplaceRDD.map(lambda x:x.split(',')).\
                            map(lambda x:[int(x[0]),x[1].encode('utf8')])

workplaceDF = spark.createDataFrame(workplaceRDD,workplaceSchema)

#Task 1. Inner join with expression and Task 2. Inner join with join column listed only once
resultDF = workerDF.join(workplaceDF,workerDF['id'] == workplaceDF['id'],how = 'inner')
resultDF.show()

#Task 3. Inner join with select projection
resultDF = workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id'], how = 'inner').\
           select(workerDF['id'],\
		          workerDF['name'],\
                  workerDF['country'],\
                  workplaceDF['department']);

resultDF.show(); 

resultDF = workerDF.join(workplaceDF,workerDF['id'] == workplaceDF['id'],how='left')
resultDF.show()

#Task 4. Inner join with SQL like query
#workerDF.registerTempTable("worker");
#workplaceDF.registerTempTable("workplaces");
#little bit safer option with overwrite mode
workerDF.write.mode('overwrite').saveAsTable('worker')
workplaceDF.write.mode('overwrite').saveAsTable('workplace')

spark.sql('SELECT w.gender,\
                  w.id,\
                  w.name,\
				  w.country,\
                  wp.department\
             FROM worker w\
        LEFT JOIN workplace wp\
		       ON w.id = wp.id').show()

#Task 5. Inner join and filter function (Scala expression)
resultDF = workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id']).filter(workplaceDF['department'] == 'qa');
resultDF.show();

#Task 6. Inner join and filter function (SQL expression)
resultDF = workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id']).filter(workplaceDF['department'] == 'qa');
resultDF.show();
			   
#Task 7. Show workers without department
resultDF = workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id'], how = 'left').filter(workplaceDF['department'].isNull());
resultDF.show();			   

#Task 8. Add column named "isQA" with "1" if worker is "qa"
from pyspark.sql import functions as F
resultDF = workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id']).\
                    withColumn("isQA",F.when(workplaceDF['department'] == 'qa', 1).otherwise(0));

resultDF.show();

#Task 9. Which department has the most workers (scala syntax)
resultDF =  workerDF.join(workplaceDF, workerDF['id'] == workplaceDF['id']).\
            groupBy(workplaceDF['department']).\
			agg({'department':'count'}).\
			withColumnRenamed('count(department)','dep_workers').\
			sort('dep_workers',ascending=False).\
			limit(1)

resultDF.show();

#Task 10. Which department has the most workers (SQL syntax)
resultDF = spark.sql('SELECT wp.department,\
                             COUNT(*) AS dep_workers\
                        FROM worker w\
                  INNER JOIN workplaces wp\
                          ON w.id = wp.id \
                    GROUP BY wp.department')

resultDF.limit(1).show();