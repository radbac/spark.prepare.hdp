lines = sc.textFile('/tamingBigData/u.data')

movies = lines.map(lambda line:(int(line.split()[1]),1))#split is () here because it is tab file, not comma separated
movies.take(10)

movieCounts = movies.reduceByKey(lambda x,y:x+y)
movieCounts.take(10)

sortedMovies = movieCounts.map(lambda (x,y):(y,x)).sortByKey().map(lambda (y,x):(x,y))
sortedMovies.take(10)

results = sortedMovies.collect()

#results = sortedMovies.take(10)

for result in results:
    print(result)

	
#nicer with broadcast variables 
lines = sc.textFile('/tamingBigData/u.data')
movieNames = sc.textFile('/tamingBigData/u.item')

movieNamesBr = movieNames.map(lambda line:(int(line.split('|')[0]),line.split('|')[1]))

nameDict = sc.broadcast(loadMovieNames())

movies = lines.map(lambda line:(int(line.split()[1]),1))#split is () here because it is tab file, not comma separated

movieCounts = movies.reduceByKey(lambda x,y:x+y)

sortedMovies = movieCounts.map(lambda (x,y):(y,x)).sortByKey(ascending=False).map(lambda (y,x):(x,y))
sortedMovies.take(10)


sortedMoviesWithNames = sortedMovies.map(lambda (movie,count):(nameDict.value[movie-1][1],count))

sortedMoviesWithNames.take(10)

results = sortedMoviesWithNames.collect()
for result in results:
    print(result)