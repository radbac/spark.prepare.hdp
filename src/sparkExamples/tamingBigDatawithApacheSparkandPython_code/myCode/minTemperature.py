lines = sc.textFile('/tamingBigData/1800.csv')

def parseLine(line):
    fields = line.split(',') 
	stationID = fields[0]
    entryType = fields[2]
    temperature = float(fields[3]) * 0.1 * (9.0 / 5.0) + 32.0
    return (stationID, entryType, temperature) 

#or
minTemps = lines.map(lambda x : x.split(',')).map(lambda field:[field[0],field[2],float(field[3])* 0.1 * (9.0 / 5.0) + 32.0]).filter(lambda x: 'TMIN' in x)

minTemps.take(5)

stationTemps = minTemps.map(lambda x:(x[0],x[2]))

stationTemps.take(10)

minTemps2 = stationTemps.reduceByKey(lambda x,y:min(x,y))

results = minTemps2.collect()

for result in results:\
	print(result[0] + " " + (str(result[1])))





