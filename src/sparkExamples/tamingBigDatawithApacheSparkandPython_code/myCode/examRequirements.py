# Write a Spark Core application in Python or Scala
# Link: http://spark.apache.org/docs/latest/rdd-programming-guide.html

pyspark --help

#Initialize a Spark application
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#initializing-spark

#Run a Spark job on YARN
#https://spark.apache.org/docs/1.1.0/cluster-overview.html


#Create an RDD
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#resilient-distributed-datasets-rdds

#Link: https://github.com/apache/spark/tree/master/examples/src/main/python/sql


#Create an RDD from a file or directory in HDFS
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#external-datasets

SparkContext.wholeTextFile

#Persist an RDD in memory or on disk
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#rdd-persistence	

#Perform Spark transformations on an RDD
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#rdd-operations

#Perform Spark actions on an RDD
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#actions

#Create and use broadcast variables and accumulators
#http://spark.apache.org/docs/latest/rdd-programming-guide.html#shared-variables

#Configure Spark properties
#https://spark.apache.org/docs/1.6.0/configuration.html
