from datetime import datetime
from dateutil.parser import parse
from pyspark import SparkConf,SparkContext

conf = SparkConf().setAppName('RADBACsales')
sc = SparkContext(conf=conf)
sc.setLogLevel('ERROR')

def printResults(result,name,firstn):
    el= '=================================' 
    print ('Results per '+name+' first for the first '+str(firstn)+' records')
    print(el)
    for line,result in enumerate(result.take(firstn),1):
        print(str(line)+": "+str(result[1])+" %.2f %.2f" %result[0])

rdd = sc.textFile('/tamingBigData/sales')
		
rdd2 = rdd.map(lambda line:line.split(',')).map(lambda x:(datetime.strptime(x[2],'%Y-%m-%d'),(float(x[4]),float(x[8]),x[13].encode('utf8'))))

rdd3 = rdd2.map(lambda x:((x[0].year,x[0].month,x[0].day),(x[1])))

rddperYear =rdd3.map(lambda x:(x[0][0],x[1])).reduceByKey(lambda x,y:(x[0]+y[0],x[1]+y[1])).map(lambda (x,y):(y,x)).sortByKey()
rddperMonth =rdd3.map(lambda x:((x[0][0],x[0][1]),x[1])).reduceByKey(lambda x,y:(x[0]+y[0],x[1]+y[1])).map(lambda (x,y):(y,x)).sortByKey()
rddperDay =rdd3.reduceByKey(lambda x,y:(x[0]+y[0],x[1]+y[1])).map(lambda (x,y):((y[0],y[1]),x)).sortByKey()

def getDict(x):
    myDict={}
    for x,y in x:
	    myDict[x]=y.encode('utf8')
		
    return myDict

datesDict = getDict(rdd.map(lambda line:line.split(',')).map(lambda x:((datetime.strptime(x[2],'%Y-%m-%d'),x[2]))).distinct().collect())	
br = sc.broadcast(datesDict)

printResults(rddperYear,'year',4)
printResults(rddperMonth,'month',5)
printResults(rddperDay,'day',5)



