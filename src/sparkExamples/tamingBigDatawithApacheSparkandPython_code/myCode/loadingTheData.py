lines = sc.textFile("/tamingBigData/u.data") 

#check the data 
lines.take(5)

# map data
ratings = lines.map(lambda x:x.split() [2])

#check new RDD
ratings.take(5)

ratings.count()
#100000


result = ratings.countByValue() # this is not RDD this is Python object


#sortedResults = collections.OrderedDict(sorted(result.items())) \
#for key, value in sortedResults.items(): \
#    print("%s %i" % (key, value))

#easier way
print(sorted(result.items()))


#run in command line
PYSPARK_PYTHON=/usr/bin/python spark-submit \
                                 --master yarn \
                                 --deploy-mode client \
                                  ratings-counter.py


