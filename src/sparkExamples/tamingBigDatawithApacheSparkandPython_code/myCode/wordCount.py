input = sc.textFile('/tamingBigData/book.txt')

words = input.flatMap(lambda line:line.split())
words.take(10)

wordCounts = words.countByValue()

for word, count in wordCounts.items(): \
    print(word + " " + str(count))

	
for word, count in wordCounts.items(): \
    cleanWord = word.encode('ascii', 'ignore') \
    if (cleanWord): \
        print(cleanWord.decode() + " " + str(count))
		
#word count better (with RE)		
import re 
input = sc.textFile('/tamingBigData/book.txt')

words = input.flatMap(lambda line:re.compile(r'\W+', re.UNICODE).split(line.lower()))

wordCounts = words.countByValue()

for word, count in wordCounts.items(): \
    print(word + " " + str(count))
	

#word count better (with RE) - Sorted
import re 
input = sc.textFile('/tamingBigData/book.txt')

words = input.flatMap(lambda line:re.compile(r'\W+', re.UNICODE).split(line.lower()))

wordCounts = words.map(lambda x:(x,1)).reduceByKey(lambda x,y:x+y)

wordCounts.take(10)

wordCountsSorted = wordCounts.map(lambda (x,y):(y,x)).sortByKey()
# or
wordCountsSorted = wordCounts.map(lambda (x,y):(y,x)).sortByKey()




		