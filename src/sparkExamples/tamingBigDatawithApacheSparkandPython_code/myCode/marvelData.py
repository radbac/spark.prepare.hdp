input = sc.textFile('/tamingBigData/Marvel-graph.txt')

names = sc.textFile('/tamingBigData/Marvel-names.txt')

namesRdd = names.map(lambda fields:(int(fields.split('\"')[0]),fields.split('\"')[1].encode("utf8")))

pairings = input.map(lambda line: (int(line.split()[0]),len(line.split())-1))
# or more pythonic way
pairings = input.map(lambda line: (int(line.split()[0]),len(line.split()[1:])))

totalFriendsByCharacter = pairings.reduceByKey(lambda x,y:x+y)

flipped = totalFriendsByCharacter.map(lambda (x,y) : (y,x))

mostPopular = flipped.max()

mostPopularName = namesRdd.lookup(mostPopular[1])[0] 

print(mostPopularName + " is the most popular superhero, with " + 
    str(mostPopular[0]) + " co-appearances.")

