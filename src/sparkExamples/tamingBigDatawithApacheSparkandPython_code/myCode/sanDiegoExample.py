import re
sc.setLogLevel("ERROR")

# function to replace ",,,," with ""
def replaceQuotesCommas(inputStr):
    return re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '', inputStr).replace('"','')

#function to prepare header	
def formatHeader(inputString):
    return inputString.lower().encode('utf8').split(',')

def CheckColumnNumbers(inputRdd):
    lng =rdd.map(lambda x:len(x)).distinct().collect()
    if len(lng) > 1:
	    return False
    else:
        return True

fname = '/tamingBigData/san_diego.csv'
rdd = sc.textFile(fname)	
header = rdd.first()
rdd =rdd.map(lambda x:replaceQuotesCommas(x))

rdd = rdd.filter(lambda x: not header in x).map(lambda line:line.split(','))
#header = formatHeader(header)

#Check RDD structure 
if CheckColumnNumbers(rdd):
    print("Structure is OK....")
else:
    print("structure is not OK...")

myllist =rdd.map(lambda x:(x[5].encode('utf8'),x[3].encode('utf8'))).\
 		 filter(lambda x:x[0]<>'').\
 		 distinct().\
 		 groupByKey().\
 		 mapValues(list).\
		 filter(lambda x:x[0]=='Tecate').\
		 collect()
mylist[0][1].sort()

for line,result in enumerate(mylist[0][1],1):
    print (str(line)+": "+result)
	
cityList = rdd.map(lambda x:x[5].encode('utf8').replace('"','')).filter(lambda x:x<>'').distinct().collect()

cityList.sort()

for line,result in enumerate(cityList,1):
    print (str(line)+": "+result)	
	
df = spark.createDataFrame(rdd,formatHeader(header))

df =df.drop(df['lat']).\
    drop(df['lon']).\
    drop(df['id']).\
    drop(df['hash']).\
    withColumn('unit',df['unit'].cast('int')).\
    withColumn('number',df['number'].cast('int'))

df.groupBy('city').count().sort('count',ascending=False).show()

df.write.format('json').save('/tamingBigData/output_sd.json')
	

df.filter(df['city']<>'').\
   groupBy('city').\
   sum('number','unit').\
   sort('sum(number)',ascending=False).\
   limit(10).\
   withColumnRenamed('sum(number)','number').\
   withColumnRenamed('sum(unit)','unit').\
   show()


