import re
from pyspark import SparkConf, SparkContext
from pyspark.sql import Row,SparkSession


def replaceQuotesCommas(inputStr):
    return re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '', inputStr).replace('"','')
	
def replaceRevenue(inputStr):
    return float(inputStr[1:-1])

conf = SparkConf()
sc = SparkContext(conf=conf)


sc.setLogLevel('ERROR')

spark = SparkSession.builder.appName('clientExample').enableHiveSupport().getOrCreate()

rddClients = sc.textFile('/tamingBigData/clients.csv')
	
rddClients2 = rddClients.map(lambda x:replaceQuotesCommas(x)).\
              map(lambda x:x.split(',')).\
	          map(lambda x:[x[0].encode('utf8'),x[1].encode('utf8'),x[2],x[3],x[5].replace('n/a',''),x[6],x[7].encode('utf8')])

rddCompany = rddClients2.map(lambda x:x[0]).\
             zipWithIndex().map(lambda (x,y):(y+1,x)).\
			 map(lambda x:Row(companyId=x[0],companyName=x[1]))
			 
rddBranch = rddClients2.map(lambda x:x[5]).distinct().\
            zipWithIndex().map(lambda (x,y):(y+1,x)).\
			map(lambda x:Row(branchId=x[0],BranchName=x[1]))
			
rddCompanyType = rddClients2.map(lambda x:x[6]).distinct().\
                 zipWithIndex().map(lambda (x,y):(y+1,x)).\
				 map(lambda x:Row(companyTypeId=x[0],companyTypeName=x[1]))
			  
dfClients = rddClients2.map(lambda x:Row(name=x[0],code=x[1],percent=x[2],revenue=x[3],year=x[4],branch=x[5],companytype=x[6])).toDF()
dfCompany = rddCompany.toDF()
dfBranch  = rddBranch.toDF()
dfCompanyType = rddCompanyType.toDF()

dfClients = dfClients.join(dfCompany,dfClients['name']==dfCompany['companyName']).\
          join(dfBranch,dfClients['branch']==dfBranch['branchName']).\
		  join(dfCompanyType,dfClients['companyType']==dfCompanyType['companyTypeName']).\
		  select(dfClients['code'],dfClients['percent'],dfClients['revenue'],dfClients['year'],\
		  dfCompany['companyId'],dfBranch['branchId'],dfCompanyType['CompanyTypeId'])


spark.sql('USE default')

dfClients.write.mode('overwrite').saveAsTable('dw_clients')
dfCompany.write.mode('overwrite').saveAsTable('dw_company')
dfBranch.write.mode('overwrite').saveAsTable('dw_branch')
dfCompanyType.write.mode('overwrite').saveAsTable('dw_company_type')
'''
spark.sql('SELECT t.companyTypeName,SUM(COALESCE(c.revenue,0)) revenue_per_year\
	FROM dw_clients c,\
		 dw_branch  b,\
		 dw_company m,\
		 dw_company_type t\
   WHERE c.companyId = m.companyId\
	 AND c.branchId  = b.branchId\
	 AND c.companyTypeId = t.companyTypeid\
   GROUP BY t.companyTypeName\
   ORDER BY 2 DESC').show()
'''

spark.sql('SELECT b.branchName,\
		          SUM(COALESCE(c.revenue,0)) AS revenue_per_year,\
                  ROUND(SUM(COALESCE(c.percent,0)),2) AS percent_per_year\
	FROM dw_clients c,\
		 dw_branch  b,\
		 dw_company m,\
		 dw_company_type t\
   WHERE c.companyId = m.companyId\
	 AND c.branchId  = b.branchId\
	 AND c.companyTypeId = t.companyTypeid\
   GROUP BY b.branchName\
   ORDER BY 3 DESC').show()
 
spark.sql('DROP TABLE dw_clients') 
spark.sql('DROP TABLE dw_company')
spark.sql('DROP TABLE dw_branch') 
spark.sql('DROP TABLE dw_company_type')  
 #spark.sql('SELECT c.branchId,SUM(c.revenue) FROM dw_clients c GROUP BY c.branchId order by 2 DESC').show()  