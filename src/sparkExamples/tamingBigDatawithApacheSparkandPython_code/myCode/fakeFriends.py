lines = sc.textFile('/tamingBigData/fakefriends.csv')

lines.take(10)

def parseLine(line): \
    fields = line.split(',') \
    age = int(fields[2]) \
    numFriends = int(fields[3]) \
    return (age, numFriends) \
	
	
# or
rdd =lines.map(lambda line:line.split(',')).map(lambda x:[x[2],x[3]])
#or or
rdd =lines.map(lambda line:[line.split(',')[2],line.split(',')[3]])


rdd.take(10)


totalsByAge = rdd.mapValues(lambda x: (x, 1)).reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1])) 

totalsByAge.take(10)

averageByAge = totalsByAge.mapValues(lambda x: x[0] / x[1])

averageByAge.collect()


results = averagesByAge.collect() 


>>> rdd = sc.parallelize([(10,(20,5)),(20,(200,1)),(30,(20,1)),(40,(20,1)),(10,(50,1))])
>>>
>>> rdd.collect()
[(10, (20, 5)), (20, (200, 1)), (30, (20, 1)), (40, (20, 1)), (10, (50, 1))]
>>> rdd2 = rdd.reduceByKey(lambda x,y:(x[0]+y[0],x[1]+y[1]))
>>> rdd2.collect()
[(40, (20, 1)), (10, (70, 6)), (20, (200, 1)), (30, (20, 1))]
>>> rdd3 = rdd2.mapValues(lambda x:x[0] / float(x[1]))
>>> rdd3.collect()
[(40, 20.0), (10, 11.666666666666666), (20, 200.0), (30, 20.0)]
