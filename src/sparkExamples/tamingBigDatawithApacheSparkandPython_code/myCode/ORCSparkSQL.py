wget http://hortonassets.s3.amazonaws.com/tutorial/data/yahoo_stocks.csv


hdfs dfs -copyFromLocal yahoo_stocks.csv /tamingBigData/.

from pyspark import HiveContext

hc = HiveContext(sc)


hc.sql("create table yahoo_orc_table (date STRING, open_price FLOAT, high_price FLOAT, low_price FLOAT, close_price FLOAT, volume INT, adj_price FLOAT) stored as orc")

hc.sql("SHOW TABLES").show()

yahoo_stocks = sc.textFile("/tamingBigData/yahoo_stocks.csv")


header = yahoo_stocks.first()

print header

dfys = yahoo_stocks.filter(lambda line:'Date' not in line)\
            .map(lambda x:x.split(','))\
			.map(lambda x:[x[0].encode('utf-8'),float(x[1]),float(x[2]),float(x[3]),float(x[4]),int(x[5])]).toDF()
 
 
 
dfys.registerTempTable("yahoo_stocks_temp")

results = hc.sql("SELECT * FROM yahoo_stocks_temp")
results.show(10)

results.write.format("orc").save("yahoo_stocks_orc")

results.write.format("orc").save('tamingBigData/hiveORC')

yahoo_stocks_orc = hc.read.format("orc").load("yahoo_stocks_orc")

yahoo_stocks_orc.registerTempTable("orcTest")

def f(x):
    print(x)
	
hc.sql("SELECT * from orcTest").collect.foreach(lambda x:f(x))

rdd1 = hc.sql("SELECT * from orcTest").rdd.map(list)


