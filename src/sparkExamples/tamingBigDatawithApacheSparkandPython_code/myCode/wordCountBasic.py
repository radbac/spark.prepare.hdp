from pyspark import SparkContext, SparkConf

conf = SparkConf().setAppName('MyFirstStandaloneApp')

sc = SparkContext(conf=conf)
sc.setLogLevel("ERROR")

rdd1 = sc.textFile('/tmp/shakespeare.txt')

rdd1.flatMap(lambda line:line.split()) \
.map(lambda word:(word,1)) \
.reduceByKey(lambda x,y: x + y) \
.map(lambda (x,y):(y,x)) \
.sortByKey(ascending=False) \
.map(lambda (x,y):(y,x))\
.filter(lambda (x,y):len(x)>3) \
.saveAsTextFile('/tmp/sh_output')

sc.stop()
