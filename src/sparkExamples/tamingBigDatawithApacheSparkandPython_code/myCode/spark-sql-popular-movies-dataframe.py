from pyspark.sql import SparkSession,Row, functions


def loadMovieNames(): 
    movieNames = {} 
    with open("u.item") as f: 
        for line in f: 
            fields = line.split('|') 
            movieNames[int(fields[0])] = fields[1] 
    return movieNames 
	
	
spark = SparkSession.builder.appName("PopularMovies").getOrCreate()

nameDict = loadMovieNames()

lines = spark.sparkContext.textFile('/tamingBigData/u.data')
#or
lines = sc.textFile('/tamingBigData/u.data')

movies = lines.map(lambda l:Row(movieID=int(l.split()[1])))

movieDataSet = movies.toDF()
#or
movieDataSet = spark.createDataFrame(movies)

topMovieIDs=movieDataSet.groupBy('movieID').count().orderBy('count',ascending=False).cache()

topMovieIDs.show()


top10 = topMovieIDs.take(10)

print("\n")
for result in top10:
    print('%s %d' % (nameDict[result[0]],result[1]))
	
spark.stop()	