sc.setLogLevel("ERROR")

from pyspark import SQLContext

sqlc = SQLContext(sc)

peopleRDD = sc.textFile('/tamingBigData/titanik.csv')
peopleRDD.collect()

people_df = sqlc.createDataFrame(peopleRDD.map(lambda line:line.split(',')),["age", "class", "name", "surname", "gender"])
people_df.show()

people_df.write.json('/tamingBigData/out_json')

df = sqlc.read.json('/tamingBigData/out_json')

df.printSchema()

df.select(df["age"],df["gender"]).show()
#OR
df.select(df.age,df.gender).show()
#OR
df.select(df[0],df[1]).show()

df2 = df.select(df.age.cast("int").alias("Age1"),df.gender.cast("string"))
df2.show()

df2.printSchema()

df.select(df.age,(df.age*2).alias("Age2")).show()

df.withColumn("Age2",df.age*2).show()

df.withColumn("age",df.age*2).show()

df.withColumnRenamed("age","AGE").show()

df1 = df.select(df['age'],df['gender'],df['class'])

df2 = df.select(df['age'],df['name'],df['surname'])

df1.join(df2,df1.age==df2.age).show()

df.drop(df.age).show()

df.dropDuplicates().show()

df.dropDuplicates(["gender"]).show()


df.withColumn("age",df["age"].cast('int')).withColumn("class",df["class"].cast("int")).describe().show()



