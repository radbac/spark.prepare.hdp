from pyspark.sql import SparkSession
from pyspark.sql import Row

spark = SparkSession.builder.appName("TEST").enableHiveSupport.getOrCreate()

spark.sql('CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive')


spark.sql("LOAD DATA LOCAL INPATH './kv1.txt' INTO TABLE src")

sqlc = SQLContext(sc)
rdd =sc.textFile('/tamingBigData/kv1.txt')

df = sqlc.createDataFrame(rdd.map(lambda line:line.split(',')).map(lambda x:[int(x[0]),x[1].encode('utf-8').replace('"','')]),['key','value'])
df.printSchema()


#u.data example
spark.sql("CREATE TABLE u_data(userid INT,movieid INT,rating INT,unixtime STRING)ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' STORED AS TEXTFILE")

spark.sql("LOAD DATA INPATH '/tamingBigData/u.data' OVERWRITE INTO TABLE u_data")

u_item = sc.textFile('/tamingBigData/u.item').map(lambda line:line.split('|')).map(lambda x:[int(x[0]),x[1].encode('utf-8'),x[2].encode('utf-8')])

u_item_df.registerTempTable("u_item")


spark.sql("SELECT i.moviename, \
                  ROUND(AVG(rating),2) rating,\
				  COUNT(*) cnt \
             FROM u_data d,\
			      u_item i \
			WHERE d.movieid = i.movieid \
		 GROUP BY i.moviename \
		   HAVING COUNT(*)>100 \
		 ORDER BY rating \
		     DESC LIMIT 20").show()
			 
spark.sql("CREATE TABLE u_item_final AS SELECT * FROM u_item")
			 
			 
			 
spark.sql("CREATE VIEW hivetest as SELECT i.moviename, \
                  ROUND(AVG(rating),2) rating,\
				  COUNT(*) cnt \
             FROM u_data d,\
			      u_item_final i \
			WHERE d.movieid = i.movieid \
		 GROUP BY i.moviename \
		   HAVING COUNT(*)>100 \
		 ORDER BY rating \
		     DESC LIMIT 20").show()			 