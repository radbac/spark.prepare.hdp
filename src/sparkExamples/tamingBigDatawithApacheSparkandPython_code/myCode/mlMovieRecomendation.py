import sys
from pyspark import SparkConf, SparkContext
from pyspark.mllib.recommendation import ALS, Rating
sc.setLogLevel('ERROR')

def loadMovieNames():
    movieNames = {}
    with open("u.item") as f:
        for line in f:
            fields = line.split('|')
            movieNames[int(fields[0])] = fields[1].decode('ascii', 'ignore')
    return movieNames

print("\nLoading movie names...")
nameDict = loadMovieNames()


file = sc.textFile('/tamingBigData/u.data')
#file.take(20)
sc.setCheckpointDir('checkpoint')

ratings = file.map(lambda x:x.split()).map(lambda x:Rating(int(x[0]),int(x[1]),float(x[2]))).cache()

rank = 10
numIterations = 20
userID = 22

model = ALS.train(ratings, rank, numIterations)

recommendations = model.recommendProducts(userID, 10)