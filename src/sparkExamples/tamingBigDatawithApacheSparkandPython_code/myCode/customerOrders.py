from pyspark import SparkContext, SparkConf

conf = SparkConf().setMaster('local').setAppName('CustomerOrders')
sc = SparkContext(conf=conf)

def extractColumnCust(line):
    fields = line.split(',')
    return(int(fields[0]),float(fields[2]))
	
input = sc.textFile('/tamingBigData/customer-orders.csv')
custOrder = input.map(extractColumnCust)
custOrderSum = custOrder.reduceByKey(lambda x,y: x + y).map(lambda (x,y):(y,x)).sortByKey(ascending=False).map(lambda (y,x):(x,y)).collect()

for resid,resamount in custOrderSum:
    print(str(resid)+" "+"%.2f" % round(resamount,2))

