from pyspark.sql import SparkSession
from pyspark.sql import Row
import collections
sc.setLogLevel('ERROR')
spark = SparkSession.builder.appName("SparkSQL").getOrCreate() 

lines = spark.sparkContext.textFile('/tamingBigData/fakefriends.csv')

linesRDD = lines.map(lambda line: line.split(','))

people = linesRDD.map(lambda x:Row(ID=x[0],name=str(x[1].encode('utf-8')),age=int(x[2]),numFriends=int(x[3])))

schemaPeople = spark.createDataFrame(people).cache() 


# or
from pyspark import SQLContext
sqlc = SQLContext(sc)

from pyspark import HiveContext
hc = HiveContext(sc)

from pyspark import Row


rddraw = sc.textFile('/tamingBigData/fakefriends.csv')

rdd = rddraw.map(lambda line:line).map(lambda x:(int(x[0]),x[1].encode('utf-8'),int(x[2]),int(x[3])))

schemaPeople = sqlc.createDataFrame(rdd,["ID","name","age","numFrieds"]).cache()

# end OR

schemaPeople.createOrReplaceTempView("people")

teenagers = spark.sql("SELECT * FROM people WHERE age >=13 AND age <19")

# different usage of SparkSQL functions

schemaPeople.groupBy("age").count().orderBy("age").show()
#OR 
spark.sql("SELECT age,count(1) from people GROUP BY age ORDER BY age").show()

