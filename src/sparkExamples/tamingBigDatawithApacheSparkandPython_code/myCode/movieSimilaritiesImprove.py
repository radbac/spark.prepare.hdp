import sys
from math import sqrt
from pyspark import SparkConf, SparkContext


conf = SparkConf().setMaster("local[*]").setAppName("MovieSimilarities")
sc = SparkContext(conf=conf)

sc.setLogLevel("ERROR")
scoreThreshold = 0.97
coOccurenceThreshold = 50
movieID =50

f = sc.textFile("/tamingBigData/u.item")
movieNames = f.map(lambda x:(int(x.split('|')[0]),x.split('|')[1].encode('utf8'))).collect()

nameDict = movieNames

data =sc.textFile("/tamingBigData/u.data")

ratings = data.map(lambda line:line.split()).map(lambda x:(int(x[0]),(int(x[1]),int(x[2])))).filter(lambda x:x[1][1] > 1)

joinedRatings = ratings.join(ratings) 

uniqueJoinedRatings=joinedRatings.filter(lambda ratings:ratings[1][0][0] <  ratings[1][1][0])


moviePairs = uniqueJoinedRatings.map(lambda (k,v):((v[0][0],v[1][0]),(v[0][1],v[1][1])))

from math import sqrt

moviePairRatingsTemp = moviePairs.groupByKey().cache()
mr2 = moviePairRatingsTemp.mapValues(lambda x:(map(lambda a: a[0]**2, list(x)),map(lambda a: a[1]**2, list(x)),map(lambda a: a[0]*a[1], list(x)),len(x)))
mr2.mapValues(lambda x:(reduce(lambda a,b: a+b, list(x[2])),reduce(lambda a,b: a+b, list(x[0])),reduce(lambda a,b: a+b, list(x[1]))))
mr3 = mr2.mapValues(lambda x:(reduce(lambda a,b: a+b, list(x[2])),sqrt(reduce(lambda a,b: a+b, list(x[0])))*sqrt(reduce(lambda a,b: a+b, list(x[1]))),x[3]))
moviePairSimilarities = mr3.mapValues(lambda x:(x[0]/float(x[1]),x[2])).cache()

filteredResults = moviePairSimilarities.filter(lambda((pair,sim)): (pair[0] == movieID or pair[1] == movieID) and sim[0] > scoreThreshold and sim[1] > coOccurenceThreshold)
results = filteredResults.map(lambda((pair,sim)): (sim, pair)).sortByKey(ascending = False).mapValues(lambda x:int(x[1])).take(10)

for result in results:
    print(nameDict[result[1]-1][1] + "\tscore: " + str(result[0][0]) + "\tstrength: " + str(result[0][1]))
