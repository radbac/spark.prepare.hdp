"""
Loading and Storing Data
"""

# =============================================================================
#                           Parallelizing
# =============================================================================

# Creating RDD by parallelizing
>>> rdd = sc.parallelize([1, 2, 3, 4, 5, 6])
>>> rdd.collect()
[1, 2, 3, 4, 5, 6]



# =============================================================================
#                             Text Files
# =============================================================================

# Read
>>> rdd = sc.textFile("file:///root/work/exam_data/dq.txt")     # local file
>>> rdd = sc.textFile("/user/root/dq.txt")                      # HDFS
>>> rdd = sc.textFile("/user/root")                    # All files in directory
>>> rdd = sc.textFile("/user/root/*.txt")              # All text files
>>> rdd.collect()
['line1', 'line2', ... ]

# Write (to directory)
>>> rdd.saveAsTextFile("file:///root/work/exam_data/dq_out")    # local dir
>>> rdd.saveAsTextFile("/user/root/dq_out")                     # HDFS dir



# =============================================================================
#                           Other Formats
# =============================================================================

#  * Read as text file
#  * Process RDD
#  * Prepare RDD output format
#  * Save as text file
