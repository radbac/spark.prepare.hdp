"""
DataFrame Actions
"""

# DataFrame for examples
>>> df = sqlContext.read.json("titanic.json")
>>> df.show()
+---+-----+-----------+---------------+------+
|Age|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
| 35|    2|     Fynney|       Joseph J|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 19|    3|    Devaney| Margaret Delia|female|
| 21|    2|       Rugg|          Emily|female|
| 45|    1|     Harris|Henry Birkhardt|  male|
|  4|    3|      Skoog|         Harald|  male|
|  4|    3|      Skoog|         Harald|  male|
+---+-----+-----------+---------------+------+


# show(n=20, truncate=True)
# Prints the first n rows to the console.
>>> df.show(4)
+---+-----+-----------+--------------+------+
|Age|Class|Family Name|    First Name|   Sex|
+---+-----+-----------+--------------+------+
| 35|    2|     Fynney|      Joseph J|  male|
| 35|    2|     Fynney|      Joseph J|  male|
| 19|    3|    Devaney|Margaret Delia|female|
| 21|    2|       Rugg|         Emily|female|
+---+-----+-----------+--------------+------+


# take(num)
# Returns the first num rows as a list of Row.
>>> df.take(2)
[Row(Age=u'35', Class=u'2', Family Name=u'Fynney', First Name=u'Joseph J', Sex=u'male'),
 Row(Age=u'35', Class=u'2', Family Name=u'Fynney', First Name=u'Joseph J', Sex=u'male')]


# collect()
# Returns all the records as a list of Row.
>>> df.collect()
[Row(Age=u'35', Class=u'2', Family Name=u'Fynney', First Name=u'Joseph J', Sex=u'male'),
 Row(Age=u'35', Class=u'2', Family Name=u'Fynney', First Name=u'Joseph J', Sex=u'male'),
 Row(Age=u'19', Class=u'3', Family Name=u'Devaney', First Name=u'Margaret Delia', Sex=u'female'),
 Row(Age=u'21', Class=u'2', Family Name=u'Rugg', First Name=u'Emily', Sex=u'female'),
 Row(Age=u'45', Class=u'1', Family Name=u'Harris', First Name=u'Henry Birkhardt', Sex=u'male'),
 Row(Age=u'4', Class=u'3', Family Name=u'Skoog', First Name=u'Harald', Sex=u'male'),
 Row(Age=u'4', Class=u'3', Family Name=u'Skoog', First Name=u'Harald', Sex=u'male')]


# describe(*cols)
# Computes statistics for numeric columns.
>>> df2 = (df.withColumn("Age", df["Age"].cast("int"))
             .withColumn("Class", df["Class"].cast("int")))
>>> df.describe().show()
+-------+------------------+------------------+
|summary|               Age|             Class|
+-------+------------------+------------------+
|  count|                 7|                 7|
|   mean|23.285714285714285|2.2857142857142856|
| stddev|15.882005390947809|0.7559289460184545|
|    min|                19|                 1|
|    max|                45|                 3|
+-------+------------------+------------------+


# count()
# Returns the number of rows in this DataFrame.
>>> df.count()
7
