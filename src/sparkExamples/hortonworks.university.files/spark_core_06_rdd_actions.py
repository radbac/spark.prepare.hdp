"""
Spark Actions
"""

# =============================================================================
#                                 Actions
# =============================================================================

# take(num)
# Take the first num elements of the RDD.
>>> rdd = sc.parallelize([1, 2, 3, 4, 5])
>>> rdd.take(2)
[1, 2]

# collect()
# Return a list that contains all of the elements in this RDD.
>>> rdd = sc.parallelize([1, 2, 3, 4, 5])
>>> rdd.collect()
[1, 2, 3, 4, 5]

# count()
# Return the number of elements in this RDD.
>>> rdd = sc.parallelize([1, 2, 3, 4, 5])
>>> rdd.count()
5

# countByKey()
# Count the number of elements for each key, and return the result
# to the master as a dictionary.
>>> rdd = sc.parallelize([("a", 1), ("b", 1), ("a", 1)])
>>> rdd.countByKey().items()
[('a', 2), ('b', 1)]

# countByValue()
# Return the count of each unique value in this RDD as a dictionary
# of (value, count) pairs.
>>> rdd = sc.parallelize([("a", 1), ("b", 1), ("a", 1)])
>>> rdd.countByValue().items()
[(('a', 1), 2), (('b', 1), 1)]

# foreach(func)
# Applies a function to all elements of this RDD (without returning any result
# to the driver program).
>>> def f(x):
...    print(x)
>>> rdd = sc.parallelize([1, 2, 3, 4, 5])
>>> rdd.foreach(lambda x: f(x))
1
2
4
5
3

# stats()
# Calculates basic statistics
>>> rdd1 = sc.parallelize([1,2,3,4,5,6,7,8,9])
>>> rdd1.stats()
(count: 9, mean: 5.0, stdev: 2.58198889747, max: 9, min: 1)
>>> rdd1.max()
9
