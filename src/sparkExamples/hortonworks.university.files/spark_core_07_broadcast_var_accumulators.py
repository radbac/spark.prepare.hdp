"""
Create and use broadcast variables and accumulators
"""

# =============================================================================
#                         Broadcast Variables
# =============================================================================

# Simple example
>>> my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> broadcast_var = sc.broadcast(my_list)
>>> broadcast_var.value
[1, 2, 3]

# Real life example
>>> rdd1 = sc.textFile("/tamingBigData/titanik.csv").map(lambda x: x.split(","))
>>> rdd1.collect()
[[u'Fynney', u' Joseph J', u' male', u' 35', u' 2'],
 [u'Devaney', u' Margaret Delia', u' female', u' 19', u' 3'], ... ]
>>> a = {"male": "unisex", "female": "unisex"}
>>> bc = sc.broadcast(a)
>>> bc.value
{'male': 'unisex', 'female': 'unisex'}
>>> rdd2 = rdd1.map(lambda x: [x[2], x[3], bc.value[x[4].strip()]])
>>> rdd2.collect()
[[u'Fynney', u' Joseph J', 'unisex'],
 [u'Devaney', u' Margaret Delia', 'unisex'], ... ]



# =============================================================================
#                            Accumulators
# =============================================================================

# Use only in Actions (not reliable in Transformations)
>>> my_accum = sc.accumulator(0)
>>> my_accum
Accumulator<id=0, value=0>
>>> my_accum.value
0
>>> sc.parallelize([1, 2, 3, 4]).foreach(lambda x: my_accum.add(1))
>>> my_accum.value
10
