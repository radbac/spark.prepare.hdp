"""
Spark Core Application
"""

# =============================================================================
#                          Spark Application
# =============================================================================

from pyspark import SparkConf, SparkContext

conf = SparkConf()
sc = SparkContext(conf=conf)
## Do Stuff
sc.stop()



# =============================================================================
#                     Submitting Spark Application
# =============================================================================
# PYTHON_DIR spark-submit RUNTIME_PAR app.py

# Minimalistic
$ spark-submit myapp.py

# More parameters
$ PYSPARK_PYTHON=/usr/bin/python spark-submit \
                                 --master yarn \
                                 --deploy-mode client \
                                 myapp.py



# =============================================================================
#                          Spark App Configuration
#                         (highest to lowest priority)
# =============================================================================

1) Inside the application
   > SparkConf object

   # Example
   conf = SparkConf().setAppName("APP_NAME").set(key, value)

2) At runtime
   > parameters of "spark-submit" command

   # Example (more: spark-submit --help)
   --name NAME                 # name of an application
   --conf PROP=VALUE           # spark.speculation=false

3) Configuration file passed to the application
   > runtime parameters in a file
   > parameter of spark-submit: --properties-file my-config.conf
   > format: key space value
   > similar to: spark-defaults.conf

4) Installation defaults
   > /etc/spark/conf/spark-defaults.conf


BEST PRACTICE (for Python API)
* set all "at runtime"



# =============================================================================
#                   Common Settings (at runtime)
# =============================================================================

--name NAME                 # name of an application

--master MASTER_URL         # yarn / local / local[*] / local[6]
--deploy-mode DEPLOY_MODE   # client / cluster

--driver-memory MEM         # 4g

--num-executors NUM         # 20
--executor-memory MEM       # 2g
--executor-cores NUM        # 1

--conf PROP=VALUE           # spark.speculation=false



# =============================================================================
#                           Monitoring
# =============================================================================

# Spark UI
http://<IP>:4040

# Spark history server
http://<IP>:18080
Ambari > Spark > Quick Links > Spark History Server
