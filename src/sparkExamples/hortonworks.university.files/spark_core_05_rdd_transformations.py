
"""
Spark Transformations

Transformation Types
   * One RDD
   * Two RDDs
   * One Pair RDD
   * Two Pair RDDs
"""

# =============================================================================
#                         One RDD Transformations
# =============================================================================

# map(f)
# Return a new RDD by applying a function to each element of this RDD.
>>> rdd = sc.textFile("dqs.txt")
>>> rdd.collect()
['line1', 'line2', ... ]
>>> rdd.map(lambda line: line.split()).collect()
[ ['word1_line1', 'word2_line1', ... ],
  ['word1_line2', 'word2_line2', ... ],  ... ]

# flatMap(f)
# Return a new RDD by first applying a function to all elements
# of this RDD, and then flattening the results.
>>> rdd = sc.textFile("dqs.txt")
>>> rdd.collect()
['line1', 'line2', ... ]
>>> rdd.flatMap(lambda line: line.split()).collect()
['word1_line1', 'word2_line1', ... , 'word1_line2', 'word2_line2', ... ]

# filter(f)
# Return a new RDD containing only the elements that satisfy a predicate.
# (gives error if empty records exists)
>>> rdd = sc.textFile("dqs.txt")
>>> rdd.filter(lambda line: line[0] == 'D').collect()

# distinct()
# Return a new RDD containing the distinct elements in this RDD.
# (remove all duplicate elements)
>>> rdd1 = sc.parallelize([1, 2, 3, 4, 1, 4])
>>> rdd1.distinct().collect()
[4, 1, 2, 3]



# =============================================================================
#                         Two RDD Transformations
# =============================================================================

# union(other)
# Return the union of this RDD and another one.
>>> rdd1 = sc.parallelize([1, 2, 3, 4])
>>> rdd2 = sc.parallelize([3, 4, 5, 6])
>>> rdd1.union(rdd2).collect()
[1, 2, 3, 4, 3, 4, 5, 6]

# intersection(other)
# Return the intersection of this RDD and another one.
>>> rdd1 = sc.parallelize([1, 2, 3, 4])
>>> rdd2 = sc.parallelize([3, 4, 5, 6])
>>> rdd1.intersection(rdd2).collect
[3, 4]

# subtract(other)
# Return each value in self that is not contained in other.
>>> rdd1 = sc.parallelize([1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> rdd2 = sc.parallelize([1, 3, 5, 7, 9])
>>> rdd1.subtract(rdd2).collect()
[2, 4, 6, 8]



# =============================================================================
#                    One Pair RDD Transformations
# =============================================================================

# reduceByKey(f)
# Merge the values for each key using an associative reduce function.
>>> rdd = sc.parallelize([("a", 1), ("b", 1), ("a", 1)])
>>> rdd.reduceByKey(lambda a,b: a + b).collect()
[('a', 2), ('b', 1)]

# groupByKey()
# Group the values for each key in the RDD into a single sequence.
>>> rdd1 = sc.parallelize([("a", 1), ("b", 1), ("a", 1)])
>>> rdd2 = rdd1.groupByKey()
>>> rdd2.collect()
[('a', <pyspark..object at 0x13e38d0>), ('b', <pyspark..object at 0x13e3910>)]
>>> rdd2.mapValues(lambda x: list(x)).collect()
[('a', [1, 1]), ('b', [1])]
# Use groupByKey() and mapValues(f) to count occurences
>>> rdd1.groupByKey().mapValues(lambda x: len(x)).collect()
[('b', 1), ('a', 2)]

# sortByKey(ascending=True)
# Sorts this RDD, which is assumed to consist of (key, value) pairs.
>>> rdd1 = sc.parallelize([("c", 1), ("b", 1), ("a", 1)])
>>> rdd1.sortByKey().collect()
[('a', 1), ('b', 1), ('c', 1)]

# mapValues(f)
# Pass each value in the key-value pair RDD through a map function without
# changing the keys.
>>> rdd1 = sc.parallelize([("a", ["apple", "banana", "lemon","ananas"]), ("b", ["grapes"])])
>>> rdd1.mapValues(lambda x: len(x)).collect()
[('a', 3), ('b', 1)]

# flatMapValues(f)
# Pass each value in the key-value pair RDD through a flatMap function
# without changing the keys
>>> rdd1 = sc.parallelize([("a", ["apple", "banana", \
                                  "lemon"]), ("b", ["grapes"])])
>>> rdd1.flatMapValues(lambda x: x).collect()
[('a', 'apple'), ('a', 'banana'), ('a', 'lemon'), ('b', 'grapes')]

# keys()
# Return an RDD with the keys of each tuple.
>>> rdd1 = sc.parallelize([(1, 2), (3, 4)]).keys()
>>> rdd1.collect()
[1, 3]

# values()
# Return an RDD with the values of each tuple.
>>> rdd1 = sc.parallelize([(1, 2), (3, 4)]).values()
>>> rdd1.collect()
[2, 4]

# zipWithIndex()
# Zips this RDD with its element indices.
>>> rdd = sc.parallelize(["a", "b", "c", "d"])
>>> rdd.collect()
['a', 'b', 'c', 'd']
>>> rdd.zipWithIndex().collect()
[('a', 0), ('b', 1), ('c', 2), ('d', 3)]

# Reordering (k, v) with map(f)
>>> rdd1 = sc.parallelize([('a', (1,2)),('a', (3,4))])
>>> rdd1.map(lambda (k, (v1,v2)): ((k,v1), v2)).collect()
[(('a', 1), 2), (('a', 3), 4)]



# =============================================================================
#                    Two Pair RDD Transformations
# =============================================================================

# Joins
#  - join() (is inner join)
#  - fullOuterJoin()
#  - leftOuterJoin()
#  - rightOuterJoin()

>>> rdd1 = sc.parallelize([("a", 1), ("b", 4)])
>>> rdd2 = sc.parallelize([("a", 2), ("a", 3)])

# join()
# Return RDD of all pairs of elements with matching keys in rdd1 and rdd2
# Each pair is (k, (v, w)) tuple, where (k, v) is in rdd1 and (k, w) is in rdd2
>>> rdd1.join(rdd2).collect()
[('a', (1, 2)), ('a', (1, 3))]

# fullOuterJoin()
# For each element (k, v) in rdd1, resulting RDD will either contain
#   - All pairs (k, (v, w)) for w in rdd2,
#   - Or the pair (k, (v, None)) if no elements in rdd2 have key k
# For each element (k, v) in rdd2, resulting RDD will either contain
#   - All pairs (k, (v, w)) for v in rdd1,
#   - Or the pair (k, (None, w)) if no elements in rdd1 have key k
>>> rdd1.fullOuterJoin(rdd2).collect()
[('a', (1, 2)), ('a', (1, 3)), ('b', (4, None))]

# leftOuterJoin()
# For each element (k, v) in rdd1, resulting RDD will either contain
#   - All pairs (k, (v, w)) for w in rdd2,
#   - Or the pair (k, (v, None)) if no elements in rdd2 have key k
>>> rdd1.leftOuterJoin(rdd2).collect()
[('a', (1, 2)), ('a', (1, 3)), ('b', (4, None))]

# rightOuterJoin()
# For each element (k, v) in rdd2, resulting RDD will either contain
#   - All pairs (k, (v, w)) for v in rdd1,
#   - Or the pair (k, (None, w)) if no elements in rdd1 have key k
>>> rdd1.rightOuterJoin(rdd2).collect()
[('a', (1, 2)), ('a', (1, 3))]


# subtractByKey(other)
# Return each (key, value) pair in self that has no pair with matching
# key in other.
>>> rdd1 = sc.parallelize([("a", 1), ("a", 2), ("b", 1), ("c", 3)])
>>> rdd2 = sc.parallelize([("a", 1), ("b", 2)])
>>> rdd1.subtractByKey(rdd2).collect()
[('c', 3)]
