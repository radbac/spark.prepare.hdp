"""
Persist an RDD in memory or on disk
"""

# Create RDD
 rdd = sc.parallelize([1,2,3,4])



# =============================================================================
#                               cache()
# =============================================================================
# Equivalent to: persist(MEMORY_ONLY_SER)

# Cache
>>> rdd.cache()

# Check if cached
>>> rdd.is_cached
True
>>> print rdd.getStorageLevel()
Memory Serialized 1x Replicated

# Remove from cache
>>> rdd.unpersist()



# =============================================================================
#                               persist()
# =============================================================================
# Storage levels:
#     MEMORY_ONLY             (default)
#     MEMORY_ONLY_SER
#     MEMORY_AND_DISK
#     MEMORY_AND_DISK_SER
#     DISK_ONLY

# Cache
>>> from pyspark import StorageLevel
>>> dir(StorageLevel)
>>> rdd.persist(StorageLevel.MEMORY_ONLY_SER)

# Check if cached
>>> rdd.is_cached
>>> print rdd.getStorageLevel()

# Remove from cache
>>> rdd.unpersist()
