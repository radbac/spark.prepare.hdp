"""
DataFrame - Loading and Storing Data

To and from:
    * RDD
    * CSV
    * Parquet / JSON
    * ORC
    * Generic text file
    * Hive
"""

# =============================================================================
#                           SQL/Hive Context
# =============================================================================

from pyspark import SQLContext
sqlc = SQLContext(sc)

from pyspark import HiveContext
hc = HiveContext(sc)



# =============================================================================
#                                 RDD
# =============================================================================

# Using Row() and toDF()
>>> from pyspark import Row
>>> df = sc.parallelize([Row(name="Alice", age=12, height=80), \
                         Row(name="Bob", age=15, height=120)]).toDF()
>>> df.show()
+---+------+-----+
|age|height| name|
+---+------+-----+
| 12|    80|Alice|
| 15|   120|  Bob|
+---+------+-----+

# Using createDataFrame()
>>> rdd = sc.parallelize([("Alice", 12, 80), ("Bob", 15, 120)])
>>> df = sqlc.createDataFrame(rdd, ["name", "age", "height"])
>>> df.show()
+-----+---+------+
| name|age|height|
+-----+---+------+
|Alice| 12|    80|
|  Bob| 15|   120|
+-----+---+------+



# =============================================================================
#                                  CSV
# =============================================================================
#   * for native read / write > external module from Databricks
#   * Method 1 - using RDD with toDF()
#   * Method 2 - using RDD with createDataFrame()   <<<<<< BEST
#   * Method 3  - using DataFrame

# Method 1 - using RDD with toDF()
>>> from pyspark import Row
>>> people_df = (sc.textFile("/tamingBigData/titanik.csv").map(lambda l: l.split(",")).map(lambda p: Row(FamilyName=p[2],FirstName=p[3],Sex=p[4],Age=int(p[0]),Class=int(p[1])))).toDF()
>>> people_df.show()
+---+-----+----------+----------------+-------+
|Age|Class|FamilyName|       FirstName|    Sex|
+---+-----+----------+----------------+-------+
| 35|    2|    Fynney|        Joseph J|   male|
| 19|    3|   Devaney|  Margaret Delia| female|
| 21|    2|      Rugg|           Emily| female|
| 45|    1|    Harris| Henry Birkhardt|   male|
|  4|    3|     Skoog|          Harald|   male|
+---+-----+----------+----------------+-------+


# Method 2 - using RDD with createDataFrame()
>>> people_rdd = sc.textFile("/sparktest/titanik.csv").map(lambda x: x.split(","))
>>> people_df = sqlc.createDataFrame(people_rdd, ["age", "class", "name", "surname", "gender"])
>>> people_df.show()
+-------+----------------+-------+---+-----+
|surname|            name|    sex|age|class|
+-------+----------------+-------+---+-----+
| Fynney|        Joseph J|   male| 35|    2|
| Fynney|        Joseph J|   male| 35|    2|
|Devaney|  Margaret Delia| female| 19|    3|
|   Rugg|           Emily| female| 21|    2|
| Harris| Henry Birkhardt|   male| 45|    1|
|  Skoog|          Harald|   male|  4|    3|
|  Skoog|          Harald|   male|  4|    3|
+-------+----------------+-------+---+-----+


# Method 3 - using DataFrame
>>> from pyspark import SQLContext
>>> sqlc = SQLContext(sc)
>>> df1 = sqlc.read.text("/sparktest/titanik.csv")
>>> df1.show(truncate=False)
+--------------------------------------+
|value                                 |
+--------------------------------------+
|Fynney, Joseph J, male, 35, 2         |
|Fynney, Joseph J, male, 35, 2         |
|Devaney, Margaret Delia, female, 19, 3|
|Rugg, Emily, female, 21, 2            |
|Harris, Henry Birkhardt, male, 45, 1  |
|Skoog, Harald, male, 4, 3             |
|Skoog, Harald, male, 4, 3             |
+--------------------------------------+
>>> from pyspark.sql.functions import split
>>> df2 = df1.select(split(df1["value"], ',').alias('new'))
>>> df2.show(truncate=False)
+--------------------------------------------+
|new                                         |
+--------------------------------------------+
|[Fynney,  Joseph J,  male,  35,  2]         |
|[Fynney,  Joseph J,  male,  35,  2]         |
|[Devaney,  Margaret Delia,  female,  19,  3]|
|[Rugg,  Emily,  female,  21,  2]            |
|[Harris,  Henry Birkhardt,  male,  45,  1]  |
|[Skoog,  Harald,  male,  4,  3]             |
|[Skoog,  Harald,  male,  4,  3]             |
+--------------------------------------------+
>>> df3 = (df2.select(df2["new"][0].alias("age"),df2["new"][1].alias("class"),df2["new"][2].alias("name"),df2["new"][3].alias("surname"),df2["new"][4].alias("gender")))
>>> df3.show()
+----------+----------------+-------+---+-----+
|FamilyName|       FirstName|    Sex|Age|Class|
+----------+----------------+-------+---+-----+
|    Fynney|        Joseph J|   male| 35|    2|
|    Fynney|        Joseph J|   male| 35|    2|
|   Devaney|  Margaret Delia| female| 19|    3|
|      Rugg|           Emily| female| 21|    2|
|    Harris| Henry Birkhardt|   male| 45|    1|
|     Skoog|          Harald|   male|  4|    3|
|     Skoog|          Harald|   male|  4|    3|
+----------+----------------+-------+---+-----+



# =============================================================================
#                         Parquet / JSON
# =============================================================================
# Works with SQLContext

>>> from pyspark import SQLContext
>>> sqlc = SQLContext(sc)

# Read
>>> people_df = sqlc.read.parquet("out_parquet")   # Parquet from dictionary
>>> people_df = sqlc.read.json("titanic.json")     # JSON from file
>>> people_df = sqlc.read.json("out_json")         # JSON from dictionary

# Write (only to dictionary)
>>> people_df.write.parquet("out_parquet")
>>> people_df.write.json("out_json")



# =============================================================================
#                                 ORC
# =============================================================================
# Works only with HiveContext!

>>> from pyspark import HiveContext
>>> hc = HiveContext(sc)

# Read
>>> people_df = hc.read.orc("out_orc")

# Write
# (space in column name > gives error!)
>>> people_df.write.orc("out_orc")



# =============================================================================
#                        Generic text file
# =============================================================================
#  See CSV files
#  * Method 1 - using RDD
#  * Method 2  - using DataFrame
#       1) load lines to DataFrame
#       2) split lines to columns using "regexp_extract"



# =============================================================================
#                                Hive
# =============================================================================

# Prepare
>>> from pyspark import HiveContext
>>> hc = HiveContext(sc)
>>> hc.sql("SHOW DATABASES").show()
+-------+
| result|
+-------+
|default|
|  my_db|
|     sd|
| xademo|
+-------+
>>> hc.sql("USE default")
>>> hc.sql("SHOW TABLES").show()
+---------+-----------+
|tableName|isTemporary|
+---------+-----------+
| my_table|      false|
+---------+-----------+

# Read data
>>> hc.sql("USE default")
>>> df = hc.sql("SELECT * FROM table1hive")
>>> df.show()
+-----------+----------+---+
|family_name|first_name|age|
+-----------+----------+---+
|     Fynney|  Joseph J| 35|
|    Devaney|Margaret D| 19|
|       Rugg|     Emily| 21|
|      Skoog|    Herald|  4|
+-----------+----------+---+

# Write data
>>> hc.sql("USE my_db")
>>> df.saveAsTable("table2hive") # df.write.saveAsTable('table2hive')
