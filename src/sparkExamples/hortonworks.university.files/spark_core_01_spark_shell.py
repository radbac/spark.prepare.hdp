"""
pyspark REPL
"""

# =============================================================================
#                          Running pyspark
# =============================================================================

# Start pyspark
$ pyspark

# Quit pyspark
>>> quit()

#  Configuration during startup
$ pyspark --help
$ pyspark --name "jz app"                     # pyspark command parameters
$ pyspark --conf "spark.executor.cores"=4     # spark properties

# Monitoring
http://IP:4040


# =============================================================================
#                            SparkContext
# =============================================================================

# SparkContext Attributes
>>> sc.appName    # Spark application name
>>> sc.master     # Spark master (local, YARN client, etc.)
>>> sc.version    # Spark version

# SparkContext Functions
>>> sc.setLogLevel("WARN")   # ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN



# =============================================================================
#                           PySpark REPL
# =============================================================================

# Help
>>> dir(sc)                   # List all atributes
>>> help(sc.setLogLevel)      # More details of specific atribute

# List of defined variables
>>> dir()         # List of in scope variables
>>> globals()     # Dictionary of global variables
>>> locals()      # Dictionary of local variables
