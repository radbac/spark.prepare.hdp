"""
DataFrame Transformations
"""

# DataFrame for examples
>>> from pyspark import SQLContext
>>> sqlc = SQLContext(sc)
>>> df = sqlc.read.json("/tamingBigData/titanic.json")
>>> df.show()
+---+-----+-----------+---------------+------+
|Age|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
| 35|    2|     Fynney|       Joseph J|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 19|    3|    Devaney| Margaret Delia|female|
| 21|    2|       Rugg|          Emily|female|
| 45|    1|     Harris|Henry Birkhardt|  male|
|  4|    3|      Skoog|         Harald|  male|
|  4|    3|      Skoog|         Harald|  male|
+---+-----+-----------+---------------+------+



# =============================================================================
#                             Getting info
# =============================================================================

>>> df.printSchema()
root
 |-- Age: string (nullable = true)
 |-- Class: string (nullable = true)
 |-- Family Name: string (nullable = true)
 |-- First Name: string (nullable = true)
 |-- Sex: string (nullable = true)



# =============================================================================
#                           Work with columns
# =============================================================================

# select(cols)
# Projects a set of expressions and returns a new DataFrame.
# (select specific columns)
>>> df.select(df["Age"], df["Sex"]).show()
>>> df.select(df.Age, df.Sex).show()
+---+------+
|Age|   Sex|
+---+------+
| 35|  male|
| 35|  male|
| 19|female|
| 21|female|
| 45|  male|
|  4|  male|
|  4|  male|
+---+------+

# select() with alias() and cast()
# alias() ... rename column
# cast()  ... set column type (int, long, float, string, ...)
>>> df2 = df.select(df["Age"].alias("Age1").cast("int"),
                    df["Sex"].alias("Sex1").cast("string"))
>>> df2.printSchema()
root
 |-- Age1: integer (nullable = true)
 |-- Sex1: string (nullable = true)

 # Use select() to add column
>>> df.select(df["Age"], (df["Age"] * 2).alias("Age*2")).show()
+---+-----+
|Age|Age*2|
+---+-----+
| 35| 70.0|
| 35| 70.0|
| 19| 38.0|
| 21| 42.0|
| 45| 90.0|
|  4|  8.0|
|  4|  8.0|
+---+-----+


# withColumn(colName, col)
# Returns a new DataFrame by adding a column or replacing the existing column
# that has the same name.
>>> df.withColumn("Age*2", df["Age"] * 2).show()      # Add column
+---+-----+-----------+---------------+------+-----+
|Age|Class|Family Name|     First Name|   Sex|Age*2|
+---+-----+-----------+---------------+------+-----+
| 35|    2|     Fynney|       Joseph J|  male| 70.0|
| 35|    2|     Fynney|       Joseph J|  male| 70.0|
| 19|    3|    Devaney| Margaret Delia|female| 38.0|
| 21|    2|       Rugg|          Emily|female| 42.0|
| 45|    1|     Harris|Henry Birkhardt|  male| 90.0|
|  4|    3|      Skoog|         Harald|  male|  8.0|
|  4|    3|      Skoog|         Harald|  male|  8.0|
+---+-----+-----------+---------------+------+-----+
>>> df.withColumn("Age", df["Age"] * 2).show()         # Modify existing column
+----+-----+-----------+---------------+------+
| Age|Class|Family Name|     First Name|   Sex|
+----+-----+-----------+---------------+------+
|70.0|    2|     Fynney|       Joseph J|  male|
|70.0|    2|     Fynney|       Joseph J|  male|
|38.0|    3|    Devaney| Margaret Delia|female|
|42.0|    2|       Rugg|          Emily|female|
|90.0|    1|     Harris|Henry Birkhardt|  male|
| 8.0|    3|      Skoog|         Harald|  male|
| 8.0|    3|      Skoog|         Harald|  male|
+----+-----+-----------+---------------+------+


# withColumnRenamed(existing, new)
# Returns a new DataFrame by renaming an existing column.
>>> df.withColumnRenamed("Age", "AGE").show()
+---+-----+-----------+---------------+------+
|AGE|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
| 35|    2|     Fynney|       Joseph J|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 19|    3|    Devaney| Margaret Delia|female|
| 21|    2|       Rugg|          Emily|female|
| 45|    1|     Harris|Henry Birkhardt|  male|
|  4|    3|      Skoog|         Harald|  male|
|  4|    3|      Skoog|         Harald|  male|
+---+-----+-----------+---------------+------+


# join(other, on=None, how=None)
# Joins with another DataFrame, using the given join expression.
>>> df1 = df.select("Age", "Class", "Sex")
>>> df1.show()
+---+-----+------+
|Age|Class|   Sex|
+---+-----+------+
| 35|    2|  male|
| 35|    2|  male|
| 19|    3|female|
| 21|    2|female|
| 45|    1|  male|
|  4|    3|  male|
|  4|    3|  male|
+---+-----+------+
>>> df2 = df.select("Age", "FamilyName", "FirstName")
>>> df2.show()
+---+----------+---------------+
|Age|FamilyName|      FirstName|
+---+----------+---------------+
| 35|    Fynney|       Joseph J|
| 35|    Fynney|       Joseph J|
| 19|   Devaney| Margaret Delia|
| 21|      Rugg|          Emily|
| 45|    Harris|Henry Birkhardt|
|  4|     Skoog|         Harald|
|  4|     Skoog|         Harald|
+---+----------+---------------+
>>> df1.join(df2, df1.Age == df2.Age).show()
+---+-----+------+---+----------+---------------+
|Age|Class|   Sex|Age|FamilyName|      FirstName|
+---+-----+------+---+----------+---------------+
| 35|    2|  male| 35|    Fynney|       Joseph J|
| 35|    2|  male| 35|    Fynney|       Joseph J|
| 35|    2|  male| 35|    Fynney|       Joseph J|
| 35|    2|  male| 35|    Fynney|       Joseph J|
| 19|    3|female| 19|   Devaney| Margaret Delia|
| 21|    2|female| 21|      Rugg|          Emily|
| 45|    1|  male| 45|    Harris|Henry Birkhardt|
|  4|    3|  male|  4|     Skoog|         Harald|
|  4|    3|  male|  4|     Skoog|         Harald|
|  4|    3|  male|  4|     Skoog|         Harald|
|  4|    3|  male|  4|     Skoog|         Harald|
+---+-----+------+---+----------+---------------+


# drop(col)
# Returns a new DataFrame that drops the specified column (only one).
>>> df.drop(df["Age"]).show()
+-----+-----------+---------------+------+
|Class|Family Name|     First Name|   Sex|
+-----+-----------+---------------+------+
|    2|     Fynney|       Joseph J|  male|
|    2|     Fynney|       Joseph J|  male|
|    3|    Devaney| Margaret Delia|female|
|    2|       Rugg|          Emily|female|
|    1|     Harris|Henry Birkhardt|  male|
|    3|      Skoog|         Harald|  male|
|    3|      Skoog|         Harald|  male|
+-----+-----------+---------------+------+


# groupBy(cols)
# Groups the DataFrame using the specified columns, so we can run
# aggregation on them.
# NOTES:
#    * groupBy() does not return a DataFrame. Instead, it returns a special
#      GroupedData object that contains various aggregation functions.
#    * available functions:  count(), sum(), max(), avg()
#    * ignores non numerical column
>>> df2 = (df.withColumn("Age", df["Age"].cast("int"))
             .withColumn("Class", df["Class"].cast("int")))
>>> df2.groupBy().avg().show()
+------------------+------------------+
|          avg(Age)|        avg(Class)|
+------------------+------------------+
|23.285714285714285|2.2857142857142856|
+------------------+------------------+
>>> df2.groupBy("Age").avg().show()
+---+--------+----------+
|Age|avg(Age)|avg(Class)|
+---+--------+----------+
| 35|    35.0|       2.0|
| 45|    45.0|       1.0|
|  4|     4.0|       3.0|
| 19|    19.0|       3.0|
| 21|    21.0|       2.0|
+---+--------+----------+
>>> df2.groupBy("Age").avg("Class").show()
+---+----------+
|Age|avg(Class)|
+---+----------+
| 35|       2.0|
| 45|       1.0|
|  4|       3.0|
| 19|       3.0|
| 21|       2.0|
+---+----------+



# =============================================================================
#                           Work with rows
# =============================================================================

# filter(condition) / where(condition)
# Filters rows using given condition.
>>> df.filter(df["Age"] > 30).show()
+---+-----+-----------+---------------+----+
|Age|Class|Family Name|     First Name| Sex|
+---+-----+-----------+---------------+----+
| 35|    2|     Fynney|       Joseph J|male|
| 35|    2|     Fynney|       Joseph J|male|
| 45|    1|     Harris|Henry Birkhardt|male|
+---+-----+-----------+---------------+----+


# dropDuplicates(subset=None) / distinct()
# Return a new DataFrame with duplicate rows removed, optionally only
# considering certain columns.
>>> df.dropDuplicates().show()
+---+-----+-----------+---------------+------+
|Age|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
|  4|    3|      Skoog|         Harald|  male|
| 21|    2|       Rugg|          Emily|female|
| 45|    1|     Harris|Henry Birkhardt|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 19|    3|    Devaney| Margaret Delia|female|
+---+-----+-----------+---------------+------+
>>> df.dropDuplicates(["Sex"]).show()
+---+-----+-----------+--------------+------+
|Age|Class|Family Name|    First Name|   Sex|
+---+-----+-----------+--------------+------+
| 19|    3|   Devaney |Margaret Delia|female|
| 35|    2|    Fynney |      Joseph J|  male|
+---+-----+-----------+--------------+------+


# sort(cols, kwargs) / orderBy(cols)
# Returns a new DataFrame sorted by the specified column(s).
>>> df2 = df.withColumn("Age", df["Age"].cast("int"))
>>> df2.sort(df2["Age"], ascending=False).show()
+---+-----+-----------+---------------+------+
|Age|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
| 45|    1|     Harris|Henry Birkhardt|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 21|    2|       Rugg|          Emily|female|
| 19|    3|    Devaney| Margaret Delia|female|
|  4|    3|      Skoog|         Harald|  male|
|  4|    3|      Skoog|         Harald|  male|
+---+-----+-----------+---------------+------+


# limit(num)
# Returns a DataFrame with a defined number of rows
>>> df.limit(1).show()
+---+-----+----------+---------+-----+
|Age|Class|FamilyName|FirstName|  Sex|
+---+-----+----------+---------+-----+
| 35|    2|    Fynney| Joseph J| male|
+---+-----+----------+---------+-----+



# =============================================================================
#                           Accessing cells
# =============================================================================

# Get Row from DataFrame
>>> age_column = df.select(df["Age"].cast("int")).collect()
>>> print age_column
[Row(Age=35), Row(Age=35), Row(Age=19), Row(Age=21), Row(Age=45), Row(Age=4), Row(Age=4)]

# Get value from one cell
>>> print age_column[1][0]
35

# Get list from column
>>> age_list = []
>>> for index in range(len(age_column)):
        age_list.append(age_column[index][0])
>>> print age_list
[35, 35, 19, 21, 45, 4, 4]



# =============================================================================
#                           Run SQL queries
# =============================================================================

>>> from pyspark import SQLContext
>>> sqlc = SQLContext(sc)
>>> sqlc.registerDataFrameAsTable(df, "my_table")
>>> sqlc.sql("SELECT * FROM my_table").show()
+---+-----+-----------+---------------+------+
|Age|Class|Family Name|     First Name|   Sex|
+---+-----+-----------+---------------+------+
| 35|    2|     Fynney|       Joseph J|  male|
| 35|    2|     Fynney|       Joseph J|  male|
| 19|    3|    Devaney| Margaret Delia|female|
| 21|    2|       Rugg|          Emily|female|
| 45|    1|     Harris|Henry Birkhardt|  male|
|  4|    3|      Skoog|         Harald|  male|
|  4|    3|      Skoog|         Harald|  male|
+---+-----+-----------+---------------+------+


>>> (sqlc
     .sql("SELECT Age AS AGE, Sex AS SEX FROM my_table WHERE Age > 40")
     .show())
+---+----+
|AGE| SEX|
+---+----+
| 45|male|
+---+----+
