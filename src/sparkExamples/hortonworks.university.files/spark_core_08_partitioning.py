"""
Data Partitioning
"""

# RDD
>>> rdd = sc.parallelize([1,2,3,4,5,6,7,8,9])


# Inspect partitioning
>>> rdd.getNumPartitions()       # Get number of partitions
6
>>> rdd.glom().collect()         # See partitions as list
[[1, 2, 3, 4], [5, 6, 7, 8, 9]]


# Increase number of partitions
# repartition(numPartitions)
# Use for increase, for decrease use coalesce() - can avoid shuffle
>>> rdd.repartition(4).getNumPartitions()
4
>>> rdd.repartition(4).glom().collect()
[[9], [], [], [1, 2, 3, 4, 5, 6, 7, 8]]


# Decrease number of partitions
# coalesce(numPartitions, shuffle=False)
>>> rdd.coalesce(1).getNumPartitions()
1
>>> rdd.coalesce(1).glom().collect()
[[1, 2, 3, 4, 5, 6, 7, 8, 9]]
