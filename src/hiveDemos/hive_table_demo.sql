CREATE TABLE IF NOT EXISTS products(id int, product String,employee String,quantity String,balance decimal,price decimal,request int,warehouse String,percentage decimal) 
COMMENT 'Product list' 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE;

LOAD DATA INPATH 'hdfs://sandbox.hortonworks.com:8020/radbac/testfiles/product.csv' OVERWRITE INTO TABLE products;
LOAD DATA INPATH '/radbac/testfiles/product.csv' OVERWRITE INTO TABLE products;
LOAD DATA INPATH '/radbac/testfiles/product.csv' INTO TABLE products2;


CREATE EXTERNAL TABLE products2(id int, product String,employee String,quantity String,balance decimal,price decimal,request int,warehouse String,percentage decimal) 
COMMENT 'Product list' 
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE
LOCATION 'hdfs://sandbox.hortonworks.com:8020/radbac/hivestorageext';


CREATE TABLE products(
  id int, 
  product string, 
  employee string, 
  quantity string, 
  balance decimal(10,0), 
  price decimal(10,0), 
  request int, 
  warehouse string, 
  percentage decimal(10,0))
COMMENT 'Product list'
  
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
  LINES TERMINATED BY '\n' 
tblproperties("skip.header.line.count"="1")
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 'hdfs://sandbox.hortonworks.com:8020/radbac/hivestorage';