# Start Ranger service
/usr/bin/ranger-admin start
/usr/bin/ranger-usersync start
/usr/bin/ranger-tagsync start 

#Start Knox
su -l knox -c "/usr/hdp/current/knox-server/bin/gateway.sh start"

#Start ZooKeeper
su - zookeeper -c "export ZOOCFGDIR=/usr/hdp/current/zookeeper-server/conf ; export ZOOCFG=zoo.cfg; source /usr/hdp/current/zookeeper-server/conf/zookeeper-env.sh ; /usr/hdp/current/zookeeper-server/bin/zkServer.sh start"

#Start HDFS
su -l hdfs -c "/usr/hdp/current/hadoop-hdfs-journalnode/../hadoop/sbin/hadoop-daemon.sh start journalnode"
su -l hdfs -c "/usr/hdp/current/hadoop-hdfs-namenode/../hadoop/sbin/hadoop-daemon.sh start namenode"
su -l hdfs -c "/usr/hdp/current/hadoop-hdfs-namenode/../hadoop/sbin/hadoop-daemon.sh start zkfc"
su -l hdfs -c "/usr/hdp/current/hadoop-hdfs-namenode/../hadoop/sbin/hadoop-daemon.sh start secondarynamenode"
su -l hdfs -c "/usr/hdp/current/hadoop-hdfs-datanode/../hadoop/sbin/hadoop-daemon.sh start datanode"

#Start YARN
su -l yarn -c "/usr/hdp/current/hadoop-yarn-resourcemanager/sbin/yarn-daemon.sh start resourcemanager"
su -l mapred -c "/usr/hdp/current/hadoop-mapreduce-historyserver/sbin/mr-jobhistory-daemon.sh start historyserver"
su -l yarn -c "/usr/hdp/current/hadoop-yarn-timelineserver/sbin/yarn-daemon.sh start timelineserver"
su -l yarn -c "/usr/hdp/current/hadoop-yarn-nodemanager/sbin/yarn-daemon.sh start nodemanager"

#Start HBase
su -l hbase -c "/usr/hdp/current/hbase-master/bin/hbase-daemon.sh start master; sleep 25"
su -l hbase -c "/usr/hdp/current/hbase-regionserver/bin/hbase-daemon.sh start regionserver"

#Start the Hive Metastore
nohup /usr/hdp/current/hive-metastore/bin/hive --service metastore>/var/log/hive/hive.out 2>/var/log/hive/hive.log &
nohup /usr/hdp/current/hive-server2/bin/hiveserver2 -hiveconf hive.metastore.uris=/tmp/hiveserver2HD.out 2 /tmp/hiveserver2HD.log #????
#su - hive -c "nohup /usr/hdp/current/hive-metastore/bin/hive --service metastore -hiveconf hive.log.file=hivemetastore.log >/var/log/hive/hivemetastore.out 2>/var/log/hive/hivemetastoreerr.log &"
#su - hive -c "/usr/hdp/current/hive-server2/bin/hiveserver2 >/var/log/hive/hiveserver2.out 2> /var/log/hive/hiveserver2.log &"

#Start WebHCat
su -l hcat -c "/usr/hdp/current/hive-webhcat/sbin/webhcat_server.sh start"

#Start Oozie
su -l oozie -c "/usr/hdp/current/oozie-server/bin/oozied.sh start"

#As a root user, execute the following command on the Hue Server
/etc/init.d/hue start

#Start Storm ????
#sudo /usr/bin/supervisorctl 
#storm-drpc RUNNING pid 9801, uptime 0:05:05
#storm-nimbus STOPPED Dec 01 06:18 PM
#storm-ui RUNNING pid 9800, uptime 0:05:05
#supervisor> start storm-nimbus
#storm-nimbus: started

#Start Kafka
/usr/hdp/current/kafka-broker/bin/kafka start

#Start the Atlas server
/usr/hdp/2.6.1.0-129/atlas/bin/atlas_start.py –port 21000
