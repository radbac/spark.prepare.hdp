# README #

Hi and welcome here. Hope you will find something useful for you.

### What is this repository for? ###

* This repository is preparation and learning material for Hortonworks exam: HDP CERTIFIED APACHE SPARK DEVELOPER. More at [Hortonworks](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/)
* Version 1.0

### How do I get set up? ###

* Set up - very simple: read and download VM image for [Hortonworks data platform](https://hortonworks.com/products/data-center/hdp/)

### Documentation ###
* [GitHub Apache Spark docs](https://github.com/apache/spark/tree/master/examples/src/main/python)
### Contacts ###

For any additional info, feel free ping me:

* [Radovan Baćović (LinkedIn profile)](https://www.linkedin.com/in/radovan-ba%C4%87ovi%C4%87-6498603/) 
* or drop an email to: radovan.bacovic@gmail.com